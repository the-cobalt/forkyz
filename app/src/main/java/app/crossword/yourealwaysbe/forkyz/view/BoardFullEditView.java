
package app.crossword.yourealwaysbe.forkyz.view;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.inject.Inject;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;

import dagger.hilt.android.AndroidEntryPoint;

import app.crossword.yourealwaysbe.puz.ClueID;
import app.crossword.yourealwaysbe.puz.Playboard.PlayboardChanges;
import app.crossword.yourealwaysbe.puz.Playboard.Word;
import app.crossword.yourealwaysbe.puz.Playboard;
import app.crossword.yourealwaysbe.puz.Position;
import app.crossword.yourealwaysbe.puz.Puzzle;

/**
 * A live view of the full playboard
 *
 * Renders the playboard on change and implements input connection with
 * soft-input.
 */
@AndroidEntryPoint
public class BoardFullEditView extends BoardEditView {
    @Inject
    public BoardFullEditView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public Position findPosition(Point point) {
        PlayboardRenderer renderer = getRenderer();
        if (renderer == null)
            return null;
        return renderer.findPosition(point);
    }

    @Override
    public float fitToView() {
        PlayboardRenderer renderer = getRenderer();
        if (renderer == null)
            return -1;

        scrollTo(0, 0);
        int w = getWidth();
        int h = getHeight();
        float scale = renderer.fitTo(w, h);
        setCurrentScale(scale);
        return scale;
    }

    @Override
    public void onPlayboardChange(PlayboardChanges changes) {
        render(changes.getCellChanges());
        ensureVisible(changes.getPreviousWord());
        super.onPlayboardChange(changes);
    }

    @Override
    public void onNewResponse(String response) {
        Playboard board = getBoard();
        if (board == null)
            return;

        getSettings().getPlayScratchMode(scratchMode -> {
            if (scratchMode) {
                if (response != null)
                    board.playScratchLetter(response.charAt(0));
            } else {
                board.playLetter(response);
            }
        });
    }

    @Override
    public void onDeleteResponse() {
        Playboard board = getBoard();
        if (board == null)
            return;

        getSettings().getPlayScratchMode(scratchMode -> {
            if (scratchMode) {
                board.deleteScratchLetter();
            } else {
                board.deleteLetter();
            }
        });
    }

    @Override
    public void render(Collection<Position> changes, boolean rescale) {
        PlayboardRenderer renderer = getRenderer();
        Playboard board = getBoard();
        if (renderer == null || board == null)
            return;

        Collection<Position> localChanges
            = (changes == null)
                ? null
                : new HashSet<>(changes);

        getSettings().getPlayScratchDisplay(displayScratch -> {
        getSettings().getPlaySuppressHintHighlighting(suppressHints -> {
            renderer.setHintHighlight(!suppressHints);

            Set<String> suppressNotesList = displayScratch
                ? Collections.emptySet()
                : null;

            executorService.execute(() -> {
                try {
                    lockRenderer();
                    Bitmap bitmap = renderer.draw(localChanges, suppressNotesList);
                    handler.post(() -> {
                        try { setBitmap(bitmap, rescale); }
                        finally { unlockRenderer(); }
                    });
                } catch (Exception e) {
                    unlockRenderer();
                    throw e;
                }
            });
        });});
    }

    @Override
    protected void onClick(Position position) {
        Playboard board = getBoard();
        if (board == null)
            return;

        if (board.isInWord(position)) {
            Word previousWord = board.setHighlightLetter(position);
            notifyClick(position, previousWord);
        } else {
            PlayboardRenderer renderer = getRenderer();
            if (renderer == null)
                return;

            Position cellPos = renderer.getUnpinnedPosition(position);
            if (cellPos != null) {
                Puzzle puz = board.getPuzzle();
                if (puz == null)
                    return;

                ClueID pinnedCID = puz.getPinnedClueID();
                Word previousWord
                    = board.setHighlightLetter(cellPos, pinnedCID);
                notifyClick(position, previousWord);
            }
        }
    }

    private void ensureVisible(Word previousWord) {
        PlayboardRenderer renderer = getRenderer();
        Playboard board = getBoard();
        if (renderer == null || board == null)
            return;

        /*
         * If we jumped to a new word, ensure the first letter is visible.
         * Otherwise, insure that the current letter is visible. Only necessary
         * if the cursor is currently off screen.
         */
        getSettings().getPlayEnsureVisible(ensureVisible -> {
            if (ensureVisible) {
                Word currentWord = board.getCurrentWord();
                Position cursorPos = board.getHighlightLetter();

                Point topLeft;
                Point bottomRight;
                Point cursorTopLeft;
                Point cursorBottomRight;

                cursorTopLeft = renderer.findPointTopLeft(cursorPos);
                cursorBottomRight = renderer.findPointBottomRight(cursorPos);

                if (
                    (previousWord != null)
                    && previousWord.equals(currentWord)
                ) {
                    topLeft = cursorTopLeft;
                    bottomRight = cursorBottomRight;
                } else {
                    topLeft = renderer.findPointTopLeft(currentWord);
                    bottomRight = renderer.findPointBottomRight(currentWord);
                }

                ensureVisible(bottomRight);
                ensureVisible(topLeft);

                // ensure the cursor is always on the screen.
                ensureVisible(cursorBottomRight);
                ensureVisible(cursorTopLeft);
            }
        });
    }
}
